<?php // This module coded to be "included" within other PHP applications.
/*
* A new "GetOpt" commandline options parser, "NEW GetOpt" - with improved error-trapping,
*    And detecting and trapping out invalid options choices at the commandline.
*
*    - "Something that should have been fixed in the original PHP library, a long time ago!"
*
* A new answer to a few of the weaknesses in PHP's "getopt()" function.
*
* THE WEAKNESSES ANSWERED:
*
* 1. How to handle invalid options ($flags),
* 2. How to handle duplicate options instances ($duplics),
*
* NOTE: There must be at least a <space> or '=' between the option-flag and the argument-value!
*
* TO USE: Just "include" this module into your project file, and then replace any instance of
*         "getopt()" with the "new-getopt()" function - and you're golden!
*
* TODO: Add additional capabilities, like user-choice of actions to handle user-errors.
*
* Though I claim this as creative copyright, under Jim S. Smith, AFWS as of (c) 2022,
*       I hereby release this source-code, licensed under GPL 2.0 - and offer it up free of any
*       "strings-attached" - except that no liability for its use or intended usage will be assumed by me.
*       - You use this script "at your own risk of loss and/or failure to perform as expected".
*/

/* DEFINED CUSTOM CONSTANTS - BIT-ENCODED FLAGS, SWITCHES. */
defined( 'IGNORE_BAD' ) || define( 'IGNORE_BAD', 0 ); // Also "false" works.
defined( 'INFORM_BAD' ) || define( 'INFORM_BAD', 1 );
defined( 'LIST_ALL_BAD' ) || define( 'LIST_ALL_BAD', 2 );
defined( 'HALT_ON_BAD' ) || define( 'HALT_ON_BAD', 4 );

defined( 'IGNORE_DUPES' ) || define( 'IGNORE_DUPES', 0 ); // Also "false" works.
defined( 'INFORM_ON_DUPES' ) || define( 'INFORM_ON_DUPES', 1 );
defined( 'LIST_ALL_DUPES' ) || define( 'LIST_ALL_DUPES', 2 );
defined( 'HALT_ON_DUPES' ) || define( 'HALT_ON_DUPES', 4 );
//defined( '' ) || define( '', '' );


// The first two function-arguments should be close enough the same as the original "getopt".
function new_getopt( $short_opts = '', $long_opts = [], $on_invalid = IGNORE_BAD, $on_duplics = IGNORE_DUPES ) {
	if ( empty( $short_opts ) && ( ! count( $long_opts ) || ! is_array( $long_opts ) ) ) {
		echo "No arguments specified!\n";
		return 22; // Linux error return code for "invalid argument".
		}

//  A stack-array, to hold the available options that are allowed.
	$these_opts = [];

	if ( ! empty( $short_opts ) ) {
		foreach ( str_split( $short_opts ) as $this_shortie ) {
			if ( $this_shortie != ':' ) {
				$these_opts[] = $this_shortie;
				}
			}
		}

	if ( is_array( $long_opts ) && count( $long_opts ) ) {
		foreach ( $long_opts as $this_longie ) {
			$these_opts[] = str_replace( ':', '', $this_longie );
			}
		}

//  if ( $on_invalid ) { . . . }

//	$found_args = []; //  Possible future use? ? ?

//  Parse the commandline options via PHP's "argv" array.
	foreach ( $GLOBALS['argv'] as $this_arg ) {

//      Only want the actual "options" switches, not the argument-values.
		if ( strlen( $this_arg ) > 1 && $this_arg[0] == '-' ) {
			$new_arg = str_replace( '-', '', $this_arg );
			$new_arg = ( strpos( $new_arg, '=' ) !== false ? strstr( $new_arg, '=', true ) : $new_arg );

//          Oops! Found an invalid option!
			if ( ! in_array( $new_arg, $these_opts ) ) {
//				$found_args[] = $new_arg;

                $this_arg = ( strpos( $this_arg, '=' ) !== false ? strstr( $this_arg, '=', true ) : $this_arg );

				// Tell the user - a return a standard Linux EC that is equivalent.
				echo "Invalid argument specified '$this_arg'!\n";

//              if ( $on_invalid == HALT_ON_BAD ) { . . . }
//              Linux error return code for "invalid argument".
				return 22;
				}
			}
		}

//  If all is good, fall-through to the regular "getopt()" function.
	return getopt( $short_opts, $long_opts );
}
