#!/usr/bin/php
<?php

// Use our new solution to the "getopt()" function's short-comings.
include_once "./inc/new-getopt.php";


// Post formatted error message.
function post_errors( $this_message ) {
    echo "\nERROR:  $this_message\n";
}

function show_usage() {
	echo "The usage text.\n\n";
}

function show_help() {
	echo "The Help text.\n\n";
}


// Set up default argument values.
$IFILE = '';
$OFILE = '';
$IRES = 90;
$COMP = 'low';
$REPLACE = 'no';

$RES = 0;

// Value boundaries.
$RES_LO = 0;
$RES_HI = 101;

// It seems PHP also has a very weak "getopt" feature! - So we use our own drop-in solution.
$theseOptions = new_getopt( 's:d:i:c:rh',
    [ 'source:', 'destination:', 'image-res:', 'compression:', 'replace', 'help' ] );

if ( ! is_array( $theseOptions ) ) {
	exit( $theseOptions );
	}

foreach ( $theseOptions as $thisOpt => $optValue ) {

//  Protect against repeated iterations of any options (first iteration used, only).
	if ( is_array( $optValue ) ) {
		post_errors( "Option '$thisOpt' was repeated! Only first instance will be used." );
		$optValue = $optValue[0];
		}

    switch ( $thisOpt ) {

        case 'source':
        case 's':
            if ( ! $IFILE = $optValue ) {
                post_errors( "Missing value for Source File!" );
                exit( 9 );
                }

            if ( ! preg_match( '!(\.pdf)$!', $IFILE ) ) {
                post_errors( "Source file path is NOT for a PDF!\n\n'$IFILE' given." );
                exit( 9 );
                }

            if ( ! file_exists( $IFILE ) ) {
                post_errors( "'$IFILE' does not exist!" );
                exit( 2 );
                }
        break;

        case 'destination':
        case 'd':
            if ( ! $OFILE = $optValue ) {
                post_errors( "Missing value for Destination File!" );
                exit( 9 );
                }

            if ( /* $OFILE != '-' && */ ! preg_match( '!(\.pdf)$!', $OFILE ) ) {
                post_errors( "Destination file path is NOT for a PDF!\n\n'$OFILE' given." );
                exit( 9 );
                }
        break;

        case 'image-res':
        case 'i':
            $IRES = (int)$optValue;

            if ( $IRES < $RES_LO || $IRES > $RES_HI ) {
                post_errors( "Bad value for Image Resolution!\n\nNeeds to be within range: $RES_LO < n < $RES_HI." );
                exit( 34 );
                }

            $RES = (int)$IRES;
        break;

        case 'compression':
        case 'c':
            if ( ! $COMP = $optValue ) {
                post_errors( "Compression-ratio requires a value! Default of 'minimum' will be used." );
                $COMP = 'min';
                }
        break;

        case 'replace':
        case 'r':
            $REPLACE = 'yes';
        break;

        case 'help':
        case 'h':
            show_help();
        break;

/* //   PHP's "getopt()"-implementation does NOT trap out invalid options. Boo-hoo!
        default :
            post_errors( 'Unrecognized option used!' );
            show_usage();
            exit( 22 );
*/
        }
    }

