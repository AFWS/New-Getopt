# New-Getopt

/*
* A new "GetOpt" commandline options parser, "NEW GetOpt" - with improved error-trapping,
*    And detecting and trapping out invalid options choices at the commandline.
*
*    - "Something that should have been fixed in the original PHP library, a long time ago!"
*
* A new answer to a few of the weaknesses in PHP's "getopt()" function.
*
* THE WEAKNESSES ANSWERED:
*
* 1. How to handle invalid options ($flags),
* 2. How to handle duplicate options instances ($duplics),
*
* NOTE: There must be at least a <space> or '=' between the option-flag and the argument-value!
*
* TO USE: Just "include" this module into your project file, and then replace any instance of
*         "getopt()" with the "new-getopt()" function - and you're golden!
*
* TODO: Add additional capabilities, like user-choice of actions to handle user-errors.
*
* Though I claim this as creative copyright, under Jim S. Smith, AFWS as of (c) 2022,
*       I hereby release this source-code, licensed under GPL 2.0 - and offer it up free of any
*       "strings-attached" - except that no liability for its use or intended usage will be assumed by me.
*       - You use this script "at your own risk of loss and/or failure to perform as expected".
*/